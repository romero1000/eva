//var socket = io.connect('http://localhost:6677', {'forceNew': true})
//http://192.168.43.209:6677
// var socket = io('http://192.168.43.209:6677', { transports: ['websocket', 'polling', 'flashsocket'] });
var socket = io.connect('https://192.168.100.6:6677', {
    'forceNew': true,
    whithCredentials: true,
    extraHeaders:{
        "my-custom-header": "Access-Control-Allow-Origin"
    },
    transports: ['websocket', 'polling', 'flashsocket']
})

// socket.on('sms_finaldiscussion', function(data){
//     render(data)
// })

function initiateSocket(room){
    console.log("Conectando a socket .....")
    if(socket && room){
        socket.emit('join', room)
    }
}
function disconnectedSocket(){
    console.log("Desconectando socket ....")
    if(socket) socket.disconnect()
}
function subscribeToChat(cb){
    if(!socket) return(true)
    socket.on('chat', msg =>{
        console.log('Evento web socket recibido')
        return cb(null, msg)
    })
}
function sendMessage(room, message, username){
    socket.emit('chat', {message, room, username})
}

function renderGroups(){
    let myList = rooms.map(function(group_name, index){
        return (`
            <li onclick="openChat(${index})" class="list-item list-group-item">
                <img class="rounded-circle" width="20%" src="https://cdn-icons.flaticon.com/png/512/2936/premium/2936774.png?token=exp=1640227699~hmac=9131e772c9385fa3a458d07f47faa131" />
                ${group_name}
            </li>
        `)
    }).join(' ')
    let ul = document.getElementById("group_list")
    ul.innerHTML = myList

}
function renderQualifications(gdatas){
    let ranks = gdatas.slice(2, gdatas.length)
    let myList = ranks.map(function(group, index){
        return (`
            <li onclick="qualify(${index})" class="list-item list-group-item">
                <img class="rounded-circle" width="20%" src="https://cdn-icons.flaticon.com/png/512/1623/premium/1623005.png?token=exp=1640227916~hmac=ca739fe1fe63c7837ac709597288f114" />
                ${group.room}
                <span class="badge badge-primary badge-pill">${group.qualification}</span>
            </li>
        `)
    }).join(' ')
    let ul = document.getElementById("qualification")
    ul.innerHTML = myList
}

var currentActivie = null
var activitieID = null

socket.on('r-messages', function(answer){
    
    render(answer.asms)
    renderQualifications(answer.groups)
    activitieID = answer.id
})

//AQUI OBTENER LOS NOMBRES DE SALAS GENERADAS POR LA VISTA ANTERIOR
var rooms = ['Discusion final', 'Dudas', 'Grupo 1', 'Grupo 2', 'Grupo 3']
var currentRoom = rooms[0]
initiateSocket(currentRoom)
renderGroups()
//renderQualifications()



function render(data){
    var html = data.map(function(sms, index){
        return (`
            <div class="message">
                <strong>${sms.username}</strong> dice:
                <p>${sms.message}</p>
            </div>
        `)
    }).join(' ')

    var div_msg = document.getElementById('message-box')
    div_msg.innerHTML = html
    div_msg.scrollTop = div_msg.scrollHeight
}

function addMessage(e){
    var message = {
        username: document.getElementById('nickname').value,
        text_sms: document.getElementById('text-msg').value
    }

    document.getElementById('nickname').style.display = 'none'
    // socket.emit('add-message', message)
    sendMessage(currentRoom, message.text_sms, message.username)
    return false
}

function openChat(e){
    currentRoom = rooms[e]
    initiateSocket(currentRoom)
    // alert(rooms[e])
    
}

function qualify(e){
    let room = rooms[e+2]
    socket.emit('savequalification', room)
    // console.log(rooms[e])
}

function changeView(){
    window.location.href='./evaluation_results.html?idActivity='+activitieID
}
