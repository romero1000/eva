# EVA

RAMA CON VERSION FINAL DEL PROYECTO
    en proceso

DESCRIPCION DE LA ARQUITECTURA
    
    La arquitectura utilizada es la de cliente servidor utilizando sockets y el protocolo https con certificados autogenerados,
    tomando en cuenta que los socket tienen una comunicacion bidireccional
    se utilizo el protocolo HTTPS para solucionar problemas de protocolos mixtos y una configuaracion de CORS par que el 
    paquete SCORM pueda hacer peticiones al chatserver o servidor externo del chat.

    Los datos de una activadad y toda informacion y recursos de la misma se almacena en una base de datos Firebase para tener pertinencia de los datos el formato SCORM esta probado en los EVA de Moodle como en Cloud Scorm y trabaja como una actividad mas incluida.

    Los detalles del Authoring Tool estaran en el articulo de grupo en Moodle.

DOCUMENTACION DE INSTALACION Y EJECUCION REQUERIDA

    1. para realizar la instalacion debe tener en primer lugar nodejs version 16.13.0 para crear el servidor externo.
    2. clone y utile la rama master de gitlab.
    3. Se requiere la generación de los certificados autogenerados con SSL desde su computador, para que funcionen el chat en scorm.
        - Estos certificados autogenerados se debe ubicar en la raiz del directorio server.

        - Para este proyecto se crearon los certificados server.cer y server.key ubicados en la raiz del directorio server (./server/servereva-cer y ./server/servereva.key).

    4. Una vez que todo este en orden ejecute el comando npm install para ejecutar las dependencias del archivo package.json
    5. Ejecute el chatserver con el comando npm start
    6. Empaquete el directorio chatpackage en formato .zip con todos los archivos en la raiz  para que funcione tanto en Moodle como  en Cloud Scorm.
    7. Ahora ya puede cargar el packeteScorm a una plataforma como Cloud Scorm o Moodle y ejecutarla.

EVALUACION 

    1. Se realizo una evalución inicial de puntajes por grupo que se mostrará al final de la actividad.

    2. Cada paticipante puede calificar el grupo que le pareción mas interesante su participación.

ALCANCE
    
    1. El autoring tools permite crear una actividad que tenga un autogenerador de grupos y tambien de la asignación de tiempos por partes para las sub-astividades (analisis de recursos, tiempo de discusiones, creacion de presentación y un tiempo extra).  

    2. Contiene salas de chat por grupos definidos al crear la actividad para las diferentes sub-actividades permitienedo comunicarse con los demas participantes de grupo mediante el chat.

Anexos

    enlace del gitlab
        https://gitlab.com/romero1000/eva.git
    
    Se tendra en una carpeta de capturas las imagenes de nuestra evaluacion inicial tambien el chat empaquetado en scorm y que este corriendo en moodle.
