const { response, query } = require('express')
var express = require('express')
var fs = require('fs')
const db = require('./config.js')
const cors = require('cors')
var app = express()
var server = require('https').Server({
    cert: fs.readFileSync('./server/server.cer', {encoding:'utf8', flag:'r'}),
    key: fs.readFileSync('./server/server.key')
},app)
var io = require('socket.io')(server, {
    cors:{
        origin: "https://192.168.100.6:6677",
        methods:["GET", "POST", "OPTIONS", "PUT", "DELETE", "PATCH", "HEAD"],
        allowedHeaders:["Access-Control-Allow-Origin"],
        credentials: true
    }
})

app.use(express.static('client'))
app.use(cors())
/* Para "application/json" */
app.use(express.json());
/* Para "application/x-www-form-urlencoded" */
app.use(express.urlencoded())

var messages = [{
    message:"Bienvenido al chat privado de socket.io",
    room:'A',
    username:"Nana"
}]

async function saveActivitie(sms, id){
    console.log("Creando----")
    let activitie = await db.collection('activities').doc(id).get();
    let activitieRef = await db.collection('activities').doc(id)
    let activitieData = activitie.data()
    activitieData.messages.push(sms)
    await activitieRef.set(
        activitieData
    )
}
async function saveQualification(smsr, id){
    
    let activitie = await db.collection('activities').doc(id).get();
    let activitieRef = await db.collection('activities').doc(id)
    let activitieData = activitie.data()
    
    for(let ele of activitieData.groups){
        if(ele.room == smsr){
            ele.qualification = ele.qualification + 1
            break
        }
    }
    await activitieRef.set(
        activitieData
    )
}

async function saveActivitietest(){
    console.log("Creando----")
    let activitieRef = await db.collection('activities').doc("NJ1ZgqhCgd4wvmLampPZ")
    let activitieData = {
        title:"Actividad de Lectura",
        description: "Deben leer todo el contenido",
        groups:[
            {
                room:"Discusion final",
                qualification:0,
                users:["Nana", "Layla", "Ana", "Jose", "Crassus", "Baras"]
            },
            {
                room:"Dudas",
                qualification:0,
                users:["Lic Crassus Marcus"]
            },
            {
                room:"Grupo 1",
                qualification:0,
                users:["Nana", "Layla", "Crassus"]
            },
            {
                room:"Grupo 2",
                qualification:0,
                users:["Ana", "Jose", "Crassus"]
            },
            {
                room:"Grupo 3",
                qualification:0,
                users:["Baras", "Lion", "Crassus"]
            }
        ],
        messages:[
            {
                message:"BIENVENIDOS",
                room:"Discusion final",
                username:"Actividad"
            },
            {
                message:"BIENVENIDOS A LAS DUDAS",
                room:"Dudas",
                username:"Dudas"
            },
            {
                message:"Le damos la bienvenida",
                room:"Grupo 1",
                username:"Grupo 1"
            },
            {
                message:"Le damos la bienvenida",
                room:"Grupo 2",
                username:"Grupo 2"
            },
            {
                message:"Le damos la bienvenida",
                room:"Grupo 3",
                username:"Grupo 3"
            }
        ]
    }
    
    await activitieRef.set(
        activitieData
    )
}

// saveActivitie()

async function getActivities(room, id){
    const activitie = await db.collection('activities').doc(id).get();
    let activitieData = activitie.data()
    //messages = activitieData.channels[0].messages
    let msg = activitieData.messages
    let asms = []
    for(let e of msg){
        if(e.room == room){
            asms.push(e)
        }
    }
    let answer = {
        asms: asms,
        groups: activitieData.groups,
        id: id
    }
    return answer
}
async function getActivity(id){
    const activitie = await db.collection('activities').doc(id).get();
    let activitieData = activitie.data()
    
    return activitieData
}


//save activity
async function saveActivity(formData){
    const activity = await db.collection('activities');
    idActivity = activity.doc().id
    activity.doc(idActivity).set(formData)
    //activity.add(formData);
    return idActivity
}

//obetener actividad 
var activityRes = ""
async function getActivity(id){
    const activity = await db.collection('activities').doc(id).get()
    let activityData = activity.data()
    console.log(activityData)
    activityRes = activityData
    return activityData
}

//get student
var estudentList = []
async function getEstudent(){
    
    const estudent = await db.collection('participants');
    const snapshot = await estudent.get();

    snapshot.forEach(element=>{
        
        let id = element.data().codigo
        let name = element.data().name
        let obj = {id, name}
        estudentList.push(obj)
    });
    
}

getEstudent()

app.get('/', function(req, res){
    res.status(200).send('Hola esta es la pagina de inicio')
})

app.post('/saveActivity',function(req, res){
    //saveActivity(req.params)
    let params = req.body;
    let id
    saveActivity(params).then(response => {
        id = response
        res.status(200).send(id)
    })
    
})

app.get('/getParticipant',function(req, res){
    
    res.status(200).send(estudentList)
})

app.get('/getActivity/:id?',function(req, res){
    let id = req.query.id
    console.log(id, 112)
    /*getActivity(id)
    res.status(200).send(activityRes)*/
    //res.status(200).send("correcto")
    //let id = req.params.id
    
    getActivity(id).then(response => {
        res.status(200).send(response)
    })
})

// io.on('connection', function(socket){
//     console.log('El nodo con IP: '+socket.handshake.address+" se ha conectado")
    
//     getActivities().then(msg =>{
//         socket.emit('sms_finaldiscussion', msg)
//     })

//     socket.on('add-message', function(data){
//         // console.log(data)
//         // messages.push(data)
//         saveActivitie(data).then(()=>{
//             getActivities().then(msg =>{
//                 socket.emit('sms_finaldiscussion', msg)
//             })
//         })
//     })
app.get('/results/:idActivity', function(req, res){
    let idActivity = req.params.idActivity;
    res.status(200).send(idActivity)
})

io.on('connection', function(socket){
    console.log('El nodo con IP: '+socket.handshake.address+" se ha conectado")
    console.log("Conectado: "+ socket.id)
    let socketRoom

    socket.on('disconnect', ()=>{
        console.log('Desconectado: '+socket.id)
    })
    socket.on('idActivity', (idActivity)=>{
        getActivity(idActivity).then(activitie =>{
            socket.emit('activitiedata', activitie)
        })
    })

    socket.on('join', (room) =>{
        console.log("El socket con id "+socket.id+"se unio a la sala "+room)
        socket.join(room)
        socketRoom = room
        getActivities(socketRoom, 'NJ1ZgqhCgd4wvmLampPZ').then(answer =>{
            io.to(socketRoom).emit('r-messages', answer)
        })
    })

    socket.on('chat', (data) =>{
        console.log(data)
        const {message, room, username} = data
        
        saveActivitie(data, 'NJ1ZgqhCgd4wvmLampPZ').then(()=>{
            getActivities(socketRoom, 'NJ1ZgqhCgd4wvmLampPZ').then(answer =>{
                io.to(socketRoom).emit('r-messages', answer)
            })
        })
    })

    socket.on('savequalification', (droom) =>{
        console.log(droom)
        
        
        saveQualification(droom, 'NJ1ZgqhCgd4wvmLampPZ').then(()=>{
            getActivities(socketRoom, 'NJ1ZgqhCgd4wvmLampPZ').then(answer =>{
                io.to(socketRoom).emit('r-messages', answer)
            })
        })
    })
})
server.listen(6677, function(){
    console.log('servidor funcionando en https://192.168.100.6:6677')
})
