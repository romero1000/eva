var admin = require("firebase-admin");

var serviceAccount = require("../serviceAccountKeys.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const db = admin.firestore()

module.exports = db

// let participantsRef = db.collection("participants")

// participantsRef.get().then((querySnapshot)=>{
//   querySnapshot.forEach(document =>{
//     console.log(document.data())
//   })
// })
