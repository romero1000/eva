$(document).ready(function(){
    //var socket = io.connect('http://localhost:6677', {'forceNew': true});

    const URL = "https://192.168.100.6:6677";
    var numEstudent
    var numSelect
    var participants
    var groups
    var messages
    var content_file;

    //var input= $('input[type=file]')[0]

    $("#input_file1").on('change', function(e){
        let fileData = new FileReader();
        let filename = $('input[type=file]')[0].files[0].name
        fileData.readAsDataURL(e.target.files[0])
        fileData.onload = () => {
            content_file = $(".content_file")
            fileShow = `
            <div>${filename}</div>
            `
            console.log(fileData)
            content_file.append(fileShow)
        }
    })

    //
    $("#amountGroup").on('change', function(e){
        generateGroup()
    })

    // Get estudent
    $.get(URL+'/getParticipant',function(data){
        showParticipant(data)
    });

    //Mostrar Participantes
    function showParticipant(dataList){
        dataList.forEach((item, index)=>{
            var studentItem = `
                <ul class="list-group list-group-flush">
                    <li class="list-group-item" style="background: #B4C0EB;">${item.name}</li>
                </ul>
            `;
            $("#studentList").append(studentItem);
        });
        numEstudent = dataList.length
        participants = dataList
        console.log(participants, numEstudent)
        getValueAmountGroup()
    }

    // obetener valores de grupo 
    function getValueAmountGroup(){
        let valueAmount = []
        valueAmount.push('')
        if(!isPrime(numEstudent)){
            for(let i = 2; i < numEstudent; i++){
                if(numEstudent % i == 0){
                    valueAmount.push(i)
                }
            }
        }else{
            for(let i = 2; i <= numEstudent/2; i++){
                valueAmount.push(i)
            }
        }
        showValueAmount(valueAmount)
    }

    // Mostrar valores de entrada
    function showValueAmount(value){    
        value.forEach((item, index)=>{
            var  amountSel = `<option>${item}</option>`;
            $("#amountGroup").append(amountSel);
        
        });
    }

    function isPrime(num){
        let cont = 2;
        let prime = true;
        while((prime) && cont!=num){
            if(num % cont == 0){
                prime = false
            }
            cont ++;
        }
        return prime
    }
    // Generar Grupo
    function generateGroup(){
        let amoutGroup = $("#amountGroup").val();
        let listGroup = []
        let listmsg = []
        let count
        if(numEstudent%amoutGroup==0){
            count = numEstudent/amoutGroup
        }else{
            count = (numEstudent/amoutGroup)-1
            
        }  
        listGroup.push({
            qualification: "",
            room: "Dudas",
            users: getNameParticipant()
        })
        listGroup.push({
            qualification: "",
            room: "Discusion final",
            users: getNameParticipant()
        })
        listmsg.push({
            message: "Bienvenido al chat",
            room: "Dudas",
            username: ""
        })
        let end = parseInt(amoutGroup);
        let init = 0
        for(let i = 0; i < count; i++){
            listGroup.push({
                qualification: "",
                room: "Grupo "+(i+1),
                users: addParticipants(init, end),
            })
            listmsg.push({
                message: "Bienvenido al chat",
                room: "Grupo "+(i+1),
                username: ""
            })
            end = end+parseInt(amoutGroup)
            init = init+parseInt(amoutGroup)
        }
        groups = listGroup
        messages = listmsg
        console.log(listGroup)
    }

    function getNameParticipant(){
        let res = []
        for(let i = 0; i < numEstudent; i++){
            res.push(participants[i].name)
        }
        return res
    }

    function addParticipants(a, b){
        let res = []
        console.log(a, b)
        if(b > numEstudent || (numEstudent-b == 2) || (numEstudent-b == 1)){
            b = numEstudent
        }
        for(let i = a; i < b; i++){
            res.push(participants[i].name)
        }
        console.log(res)
        return res
    }

    var timeSelect = [
        {time: ''},
        {time: '5'},{time: '6'},{time: '7'},{time: '8'},{time: '9'},{time: '10'},
        {time: '11'},{time: '12'},{time: '13'},{time: '14'},{time: '15'},
        {time: '16'},{time: '17'},{time: '18'},{time: '19'},{time: '20'},
        {time: '21'},{time: '22'},{time: '23'},{time: '24'},{time: '25'},
        {time: '26'},{time: '27'},{time: '28'},{time: '28'},{time: '30'},
    ];
    //

    // 
    timeSelect.forEach((item, index)=>{
        var timeSel = `<option>${item.time}</option>`;
        $("#time1").append(timeSel);
        $("#time2").append(timeSel);
        $("#time3").append(timeSel);
        $("#time4").append(timeSel);
    });

    var dataAtivity;

    $("#btn_send").on("click",function(evt){
        evt.preventDefault();
        var formData = new FormData(document.getElementById("formi"));
        var title = $("#acivityTitle").val();
        var description = $("#activityDescription").val()
        var amoutGroup = $("#amountGroup").val();
        var analysisTime = $("#time1").val();
        var discussionTime = $("#time2").val();
        var presentationTime = $("#time3").val();
        var extraTime = $("#time4").val();
        var nameFile1= $('input[type=file]')[0].files[0].name;
        console.log(nameFile1)
        /*var nameFile2= $('input[type=file]')[1].files[0].name;
        var nameFile3= $('input[type=file]')[2].files[0].name;*/
        /*
        console.log(formData);
        dataAtivity = {amout, time1, time2, time3, time4, nameFile1, nameFile2, nameFile3}
        console.log(dataAtivity);*/
        //window.location.href='http://localhost:6677/'
        dataAtivity = {title, description, amoutGroup, analysisTime, discussionTime, presentationTime, extraTime, nameFile1, groups, messages}
        $.post(URL+'/saveActivity', dataAtivity, function(data,status){
            window.location.href='./chatView.html?idActivity='+data+'&analysisTime='+analysisTime+'&discussionTime='+discussionTime+'&presentationTime='+presentationTime
            console.log(data)
        });      

    });
});

