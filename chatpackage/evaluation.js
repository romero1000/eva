var socket = io.connect('https://192.168.100.6:6677', {
    'forceNew': true,
    whithCredentials: true,
    extraHeaders:{
        "my-custom-header": "Access-Control-Allow-Origin"
    },
    transports: ['websocket', 'polling', 'flashsocket']
})

var queryString = window.location.search;
var params = new URLSearchParams(queryString)
var idActivity = params.get("idActivity")
console.log("RECUPERANDO ID")
console.log(idActivity)
socket.emit('idActivity', idActivity)
socket.on('activitiedata', (activitie)=>{
    console.log(activitie)
    renderChartByCalifications(activitie)
})

function renderChartByCalifications(activitie){
    let ranks = activitie.groups.slice(2, activitie.groups.length)
    var xValues = [];
    var barColors = [];
    var yValues = [];

    while (barColors.length < ranks.length-2) {
        do {
            var color = Math.floor((Math.random()*1000000)+1);
        } while (barColors.indexOf(color) >= 0);
        barColors.push("#" + ("000000" + color.toString(16)).slice(-6));
    }
    for(let elm of ranks){
        xValues.push(elm.room)
        yValues.push(elm.qualification)
    }
    console.log("COLORES")
    console.log(xValues)
    console.log(yValues)
    console.log(barColors)

    new Chart("myChart", {
        type: "bar",
        data: {
            labels: xValues,
            datasets: [{
                backgroundColor: barColors,
                data: yValues
            }]
        },
        options: {
            legend: {display: false},
            title: {
                display: true,
                text: "Grafico de barras de calificaciones Por Grupos"
            }
        }
    });
    new Chart("myChart2", {
        type: "pie",
        data: {
          labels: xValues,
          datasets: [{
            backgroundColor: barColors,
            data: yValues
          }]
        },
        options: {
          title: {
            display: true,
            text: "Grafico de pastel de calificaciones por grupos"
          }
        }
      });
}

// $(document).ready(function(){
//     console.log("INGRESANDO")
//     console.log(idActivity)
//     const Url = 'https://localhost:6677/results/'+idActivity
//     $.get(Url, function(data, status){
//         console.log(`${data}`)
//     })
//     // $.ajax({
//     //     url: Url,
//     //     type: "GET",
//     //     success: function(result){
//     //         console.log(result)
//     //     },
//     //     error:function(error){
//     //         console.log(error)
//     //     }
//     // })
// })

// const Http = new XMLHttpRequest();
// const url='https://localhost:6677/results/'+idActivity;
// Http.open("GET", url);
// Http.send();
// function callOtherDomain() {
//     if(Http) {
//       Http.open('GET', url, true);
//       Http.onreadystatechange = handler;
//       Http.send();
//     }
// }

// Http.onreadystatechange = (e) => {
//   console.log(Http.responseText)
// }